.. DepthPredictor documentation master file, created by
   sphinx-quickstart on Fri Aug 27 12:54:40 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DepthPredictor's documentation!
==========================================

.. toctree::
   module_features
   module_utils
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
