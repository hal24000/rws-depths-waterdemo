import os
import sys

import streamlit as st

module_path = os.path.abspath(os.path.join(".."))
sys.path.append(module_path + "/src")

from models.features import Covadem


def run_page_ksvd(page_title):
    st.header(page_title)
    st.markdown("")
    select_region = st.sidebar.selectbox("Select region", list(range(2529)), 220)

    covadem = Covadem()
    fig_ksvd = covadem.plot_ksvd(select_region)
    before_ksvd, after_ksvd, fig_total_ksvd = covadem.get_ksvd_metrics()

    c1_1, c1_2, c1_3 = st.columns((4, 1, 1))
    c1_1.pyplot(fig_total_ksvd)
    c1_2.metric("Before kSVD", f"{before_ksvd}%")
    c1_3.metric(
        "After kSVD",
        f"{after_ksvd}%",
        f"{after_ksvd - before_ksvd}",
        delta_color="inverse",
    )
    c1_1.pyplot(fig_ksvd)
