import os
import sys
from datetime import datetime

module_path = os.path.abspath(os.path.join(".."))
sys.path.append(module_path + "/src")

import pandas as pd
import streamlit as st
from streamlit_folium import folium_static

from models.features import Covadem
from models.plot import plot_pred, plot_river
from models.predict import get_pred_gdf
from models.utils import get_npArray
from setup.database import db_con

db = db_con()


def run_page_main():
    st.markdown("")

    total_filter = get_npArray("613f8466475a9d36405c24a3")
    metadata = pd.DataFrame(
        db["kk_depths_metadata"].find({}, {"_id": 0})
    )  # (489285, 2), 952 dates
    date_list = pd.to_datetime(
        metadata[total_filter]
        .sort_values("datetime", ascending=False)["datetime"]
        .unique()
    )  # 752 dates
    date_list = [str(x.date()) for x in date_list]

    select_region = st.sidebar.selectbox("Select region", list(range(2529)), 220)
    select_date = st.sidebar.selectbox("Select date", date_list, 1)
    select_date = datetime.strptime(select_date, "%Y-%m-%d")

    with st.expander("Data availability", True):
        fig_river = plot_river(select_date)
        folium_static(fig_river)

    with st.expander("Data Imputer", True):
        covadem = Covadem()
        fig_ksvd = covadem.plot_ksvd(select_region)
        before_ksvd, after_ksvd, fig_total_ksvd = covadem.get_ksvd_metrics()

        c1_1, c1_2, c1_3 = st.columns((4, 1, 1))
        c1_1.pyplot(fig_total_ksvd)
        c1_2.metric("Before kSVD", f"{before_ksvd}%")
        c1_3.metric(
            "After kSVD",
            f"{after_ksvd}%",
            f"{after_ksvd - before_ksvd}",
            delta_color="inverse",
        )
        c1_1.pyplot(fig_ksvd)

    with st.expander("Recommended Path", True):
        gdf = get_pred_gdf(select_date)
        fig_pred_deep = plot_pred(gdf, "deep")
        folium_static(fig_pred_deep)

    with st.expander("Shallow Path", True):
        gdf = get_pred_gdf(select_date)
        fig_pred_shallow = plot_pred(gdf, "shallow")
        folium_static(fig_pred_shallow)
