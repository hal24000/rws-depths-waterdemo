import os
import pickle
import sys

module_path = os.path.abspath(os.path.join(".."))
sys.path.append(module_path + "/src")

import gridfs
import numpy as np
from bson.binary import Binary
from bson.objectid import ObjectId

from tensorflow import keras

from setup.database import db_con


db = db_con()


def clean_fs_chunks():
    """Take fs files and remove fs chunks that are not present in fs files."""
    query = []
    fs_files = [d["_id"] for d in list(db["fs.files"].find({}, {}))]
    for file in fs_files:
        query.append({"files_id": {"$ne": file}})
    db["fs.chunks"].remove({"$and": query})


def get_npArray(object_id: str) -> np.array:
    """Takes object id and returns np array

    Args:
        object_id: Object id of np array being retrieved

    Returns:
        Target numpy array
    """
    a = ObjectId(object_id)
    fs = gridfs.GridFS(db)
    npArray = pickle.loads(fs.get(a).read())
    return npArray


def store_npArray(npArray: np.array, filename: str):
    """Takes numpy array and stores in MongoDB using fs chunking.

    Args:
        npArray: Numpy array
        filename: New file name
    """
    dataBSON = Binary(pickle.dumps(npArray, protocol=-1), subtype=128)
    fs = gridfs.GridFS(db)
    object_id = fs.put(dataBSON, filename=filename)
    print(object_id)


def store_model(model_name: str, model: str):
    """Takes h5 model and stores in MongoDB

    Args:
        model_name: Model name. E.g. "21/09/14 Keras model Kalman 6W lookback"
        model: Locally saved model
    """
    pickled_model = pickle.dumps(model)
    info = db["kk_rws_models"].insert_one(
        {"model_name": model_name, "model": pickled_model}
    )
    print(info)


# def get_model():
#     model = keras.models.load_model("models/best_model_kalman_6Wlookback.h5")
#     return model
