"""
Various utilities related to the data processing of the MGD2 project
Copyright 2020 HAL24K
"""

import numpy as np
import pandas as pd


# pylint: disable=R0914
def calculate_water_levels(
    measurement_df: pd.DataFrame, betrekking_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Calculate the water level interpolation based on the betrekkingslijnen.

    Args:
        measurement_df (pd.DataFrame): dataframe indexed with the lowercase location names
                                       with the 'mNAP' column with level values
                                       e.g. pd.DataFrame([{'mNAP': 10.76}, {'mNAP': 9.70}],
                                                         index=['lobith', 'nijmegen haven'])
                                       6-ish entries
        betrekking_df (pd.DataFrame): model output for the betrekkingslijnen

    Returns:
        pd.DataFrame indexed by km-raai with interpolated water level values
    """

    location_model_df = betrekking_df[
        betrekking_df["peilschaal op LMW-punt"].notna()
    ].copy()
    location_model_df["peilschaal op LMW-punt"] = location_model_df[
        "peilschaal op LMW-punt"
    ].map(lambda x: x.lower())
    location_model_df.set_index("peilschaal op LMW-punt", inplace=True)

    bet_df = betrekking_df.loc[:, betrekking_df.columns[1:]]
    bet_df.set_index("km-raai", inplace=True)

    # make sure the locations are in order
    location_model_df.sort_values(by="km-raai", inplace=True, ascending=True)

    # which betrekkingslijene columns to use
    min_df = (
        (
            location_model_df.drop("km-raai", axis=1)
            - measurement_df.reindex(location_model_df.index).values
        )
        .abs()
        .idxmin(axis=1)
        .dropna()
    )

    level_df = []

    # loop through all location from downriver up
    for row in list(range(len(measurement_df) - 1, 0, -1)):
        # find kms of the locations
        ind_down = location_model_df.loc[min_df.index[row], "km-raai"]
        ind_up = location_model_df.loc[min_df.index[row - 1], "km-raai"]
        # determine betrekkings column to use
        col = min_df.iloc[row]
        # remember km markers
        model_inds = bet_df.loc[ind_up:ind_down, col].index
        # load the model betrekkingslijnen
        model_vals = bet_df.loc[ind_up:ind_down, col].values

        # calculate the coefficient by which to multiply the betrekkingslijnen
        model_diff = model_vals[0] - model_vals[-1]
        meas_min = measurement_df.loc[min_df.index[row], "mNAP"]
        meas_max = measurement_df.loc[min_df.index[row - 1], "mNAP"]
        meas_diff = meas_max - meas_min
        coeff = meas_diff / model_diff

        # calculate the interpolated water levels
        levels = meas_min + coeff * (model_vals - np.min(model_vals))

        out_df = pd.DataFrame(levels, index=model_inds)
        out_df.columns = ["level_mNAP"]

        level_df.append(out_df)

    level_df = pd.concat(level_df)
    level_df.sort_index(inplace=True)
    level_df = level_df[~level_df.index.duplicated()]

    return level_df
