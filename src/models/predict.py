import os
import sys

module_path = os.path.abspath(os.path.join(".."))
sys.path.append(module_path + "/src")

import numpy as np
import pandas as pd
from geopandas import GeoDataFrame
from shapely.geometry import shape
import shapely.geometry

from models.features import Training
from models.utils import get_npArray
from setup.database import db_con


db = db_con()
# from models.utils import get_model

# trained_model = get_model()


def predict_depths(select_date):
    total_filter = get_npArray("613f8466475a9d36405c24a3")

    metadata = pd.DataFrame(
        db["kk_depths_metadata"].find({}, {"_id": 0})
    )  # (489285, 2), 952 dates
    filtered_metadata = metadata[total_filter].reset_index(drop=True)
    date_list = pd.to_datetime(
        filtered_metadata.sort_values("datetime", ascending=False)["datetime"].unique()
    )  # 752 dates
    feats = get_npArray("6128c7da66dbac07359f43c9")
    targs = get_npArray("6128c81266dbac07359f8632")
    shipping_data = get_npArray("6127c1bfd5697d6748db6540")  # (489285, 42, 1)

    past_water_features = get_npArray("6127dc22d5697d6748db67e8")  # (489285, 42, 1)
    future_water_features = get_npArray("6127dc26d5697d6748db6b40")  # (489285, 4, 1)
    region_meta = pd.DataFrame(db["kk_region_meta"].find({}, {"_id": 0}))

    past_water = past_water_features[total_filter]
    future_water = future_water_features[total_filter]

    past_ships = shipping_data[total_filter]
    reg_meta = region_meta.values[total_filter]

    # filter by selected date
    select_idx = filtered_metadata[filtered_metadata["datetime"] == select_date].index
    select_triplets = filtered_metadata[filtered_metadata["datetime"] == select_date][
        "region_triplet"
    ].tolist()
    select_feats = feats[select_idx]
    select_targs = targs[select_idx]

    test_feats = feats[select_idx]
    test_targs = targs[select_idx]
    test_past_w = past_water[select_idx]
    test_future_w = future_water[select_idx]
    test_past_sh = past_ships[select_idx]
    test_meta = reg_meta[select_idx]

    feature_statistics = pd.DataFrame(db["kk_feature_statistics"].find({}, {"_id": 0}))
    tr_mean, tr_std = feature_statistics["tr_mean"][0], feature_statistics["tr_std"][0]
    w_mean, w_std = feature_statistics["w_mean"][0], feature_statistics["w_std"][0]
    sh_mean, sh_std = feature_statistics["sh_mean"][0], feature_statistics["sh_std"][0]
    meta_mean, meta_std = np.array(feature_statistics["meta_mean"][0]), np.array(
        feature_statistics["meta_std"][0]
    )

    test_feats = (test_feats - tr_mean) / tr_std
    test_targs = (test_targs - tr_mean) / tr_std
    test_past_w = (test_past_w - w_mean) / w_std
    test_future_w = (test_future_w - w_mean) / w_std
    test_past_sh = (test_past_sh - sh_mean) / sh_std
    test_meta = (test_meta - meta_mean) / meta_std

    preds = trained_model.predict(
        [test_feats, test_past_w, test_future_w, test_past_sh, test_meta]
    )
    preds = (preds * tr_std) + tr_mean
    test_targs = (test_targs * tr_std) + tr_mean

    res = pd.DataFrame(columns=["region_triplet", "region", "pred", "target"])
    lst_dict = []
    triplet_count = 0
    for triplet in select_triplets:
        lst_dict.append(
            {
                "region_triplet": triplet,
                "region": triplet * 3,
                "pred": preds[triplet_count][0],
                "target": test_targs[triplet_count][0],
            }
        )
        lst_dict.append(
            {
                "region_triplet": triplet,
                "region": triplet * 3 + 1,
                "pred": preds[triplet_count][1],
                "target": test_targs[triplet_count][1],
            }
        )
        lst_dict.append(
            {
                "region_triplet": triplet,
                "region": triplet * 3 + 2,
                "pred": preds[triplet_count][2],
                "target": test_targs[triplet_count][2],
            }
        )
        triplet_count += 1

    res = pd.DataFrame(lst_dict)
    res["diff"] = res["pred"] - res["target"]
    res["pred_deep"], res["pred_shallow"] = 0, 0
    res.loc[res.groupby("region_triplet")["pred"].idxmax(), "pred_deep"] = 1
    res.loc[res.groupby("region_triplet")["pred"].idxmin(), "pred_shallow"] = 1

    training = Training()
    reg_gdf = training.create_reg_gdf()
    reg_gdf = reg_gdf.rename(columns={"FID": "region"})
    reg_gdf = pd.merge(reg_gdf, res.drop("region_triplet", axis=1), on="region")
    reg_gdf["date"] = select_date
    return reg_gdf


def predict_all_dates(notebook):
    if notebook:
        from tqdm.notebook import tqdm
    else:
        from tqdm import tqdm

    db["kk_rws_pred_keras"].drop()

    total_filter = get_npArray("613f8466475a9d36405c24a3")
    metadata = pd.DataFrame(
        db["kk_depths_metadata"].find({}, {"_id": 0})
    )  # (489285, 2), 952 dates
    date_list = pd.to_datetime(
        metadata[total_filter]
        .sort_values("datetime", ascending=False)["datetime"]
        .unique()
    )  # 752 dates
    for date in tqdm(date_list):
        gdf = predict_depths(date)
        # convert to geojson point object
        gdf["geometry"] = gdf["geometry"].apply(lambda x: shapely.geometry.mapping(x))
        db["kk_rws_pred_keras"].insert_many(gdf.to_dict("records"))


def get_pred_gdf(select_date):
    cursor = db["kk_rws_pred_keras"].find({"date": select_date}, {"_id": 0})
    df = pd.DataFrame(cursor)
    df["geometry"] = df["geometry"].apply(lambda x: shape(x))
    gdf = GeoDataFrame(df, crs="EPSG:28992", geometry=df["geometry"])
    return gdf
