import os
import sys

module_path = os.path.abspath(os.path.join(".."))
sys.path.append(module_path + "/src")

import folium
import pandas as pd
from folium import IFrame
from shapely.geometry import mapping

from models.features import Training
from setup.database import db_con


db = db_con()


def plot_river(select_date):
    training = Training()
    reg_gdf = training.create_reg_gdf()
    metadata = pd.DataFrame(
        db["kk_depths_metadata"].find({}, {"_id": 0})
    )  # (489285, 2)
    region_list = metadata[metadata["datetime"] == select_date][
        "region_triplet"
    ].tolist()

    fig = folium.Map(
        location=[51.887571, 5.464441],
        zoom_start=9.4,
        tiles="cartodbpositron",
    )

    for i, (ind, row) in enumerate(reg_gdf.to_crs(epsg="4326").iterrows()):
        html = f"""
        Region ID: {row['FID']}<br>
        Triplet ID: {row['region_triplet']}
        """
        iframe = IFrame(html=html, width=200, height=70)
        popup = folium.Popup(iframe)

        if row["region_triplet"] in region_list:
            sf = lambda x: {
                "fillColor": "#ff7f0e",
                "color": "#ff7f0e",
                "fillOpacity": 0.5,
            }
        else:
            sf = lambda x: {
                "fillColor": "#0000FF",
                "color": "#0000FF",
                "fillOpacity": 0.2,
            }
        gj = folium.GeoJson(mapping(row["geometry"]), style_function=sf)
        gj.add_child(popup)
        gj.add_to(fig)
    return fig


def plot_pred(gdf, view):
    fig = folium.Map(
        location=[51.887571, 5.464441],
        zoom_start=9.4,
        tiles="cartodbpositron",
    )

    for i, (ind, row) in enumerate(gdf.to_crs(epsg="4326").iterrows()):
        html = f"""
        Region ID: {row['region']}<br>
        Triplet ID: {row['region_triplet']}
        """
        iframe = IFrame(html=html, width=200, height=70)
        popup = folium.Popup(iframe)

        if view == "deep":
            if row["pred_deep"] == 1:
                sf = lambda x: {
                    "color": "#9FE2BF",
                    "opacity": 0.8,
                    "fillColor": "#9FE2BF",
                    "fillOpacity": 0.8,
                }
            else:
                sf = lambda x: {
                    "color": "#0000FF",
                    "opacity": 0.3,
                    "fillColor": "#0000FF",
                    "fillOpacity": 0.3,
                }
        elif view == "shallow":
            if row["pred_shallow"] == 1:
                sf = lambda x: {
                    "color": "#DE3163",
                    "opacity": 0.8,
                    "fillColor": "#DE3163",
                    "fillOpacity": 0.8,
                }
            else:
                sf = lambda x: {
                    "color": "#0000FF",
                    "opacity": 0.3,
                    "fillColor": "#0000FF",
                    "fillOpacity": 0.3,
                }
        gj = folium.GeoJson(mapping(row["geometry"]), style_function=sf)
        gj.add_child(popup)
        gj.add_to(fig)
    return fig
