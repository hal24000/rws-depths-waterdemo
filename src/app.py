import os
import sys

import streamlit as st

module_path = os.path.abspath(os.path.join(".."))
sys.path.append(module_path + "/src")

from pages.page_main import run_page_main
from pages.page_ksvd import run_page_ksvd
from setup.layout import setup_page


setup_page("River Level Prediction")
# page_titles = ["Main", "Missing Data Imputer"]
# select_page = st.sidebar.selectbox("Select page", page_titles)

run_page_main()

with st.sidebar:
    st.header("")
    st.info(
        """
    ### :information_source: River Level Prediction
    - Neural networks to predict river depths.
    """
    )
